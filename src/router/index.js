import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Components from '@/components/Components'
import Conditionals from '@/components/Conditionals'
import Instance from '@/components/Instance'
import Filters from '@/components/Filters'
import Computed from '@/components/Computed'
import DOM from '@/components/DOM'
import VueFile from '@/components/VueFile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/components',
      name: 'Components',
      component: Components
    },
    {
      path: '/conditionals',
      name: 'Conditionals',
      component: Conditionals
    },
    {
      path: '/instance',
      name: 'Instance',
      component: Instance
    },
    {
      path: '/filters',
      name: 'Filters',
      component: Filters
    },
    {
      path: '/computed',
      name: 'Computed',
      component: Computed
    },
    {
      path: '/dom',
      name: 'DOM',
      component: DOM
    },
    {
      path: '/vuefile',
      name: 'VueFile',
      component: VueFile
    }
  ]
})
